from django.test import TestCase, Client

class MoviesTestCase(TestCase):
    def setUp(self):
        client = Client()

    def test_route_movies_index_redirect_correctly(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_route_movies_listing_redirect_correctly(self):
        response = self.client.get('/films')
        self.assertEqual(response.status_code, 200)

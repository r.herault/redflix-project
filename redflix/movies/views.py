from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from .models import *
from .forms import *

def index(request):
  latest_movie_list = Movie.objects.order_by('-pub_date')[:4]

  return render(request, 'movies/pages/index.html', {'latest_movie_list': latest_movie_list})

def listing(request):
  movie_list = Movie.objects.order_by('-pub_date')
  actor_list = ActorMovie.objects.all()
  genre_list = GenreMovie.objects.all()

  return render(request, 'movies/pages/listing.html', {'movie_list': movie_list, 'actor_list': actor_list, 'genre_list': genre_list})

# def check_group():
#     return request.user.has_perm('app_name.can_add_cost_price')

# @user_passes_test(check_group)
@login_required
def add(request):
    if request.method == 'POST':
        form = MovieForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('/films')
    form = MovieForm()

    return render(request, 'movies/pages/add.html', {'form': form})

# @user_passes_test(check_group)
@login_required
def edit(request, pk):
    movie = get_object_or_404(Movie, pk=pk)
    form = MovieForm(request.POST or None, instance=movie)
    if form.is_valid():
        form.save()
        return redirect('/films')
    return render(request, 'movies/pages/edit.html', {'form': form})

# @user_passes_test(check_group)
@login_required
def delete(request, pk):
    movie = get_object_or_404(Movie, pk=pk)
    if request.method == 'POST':
        movie.delete()
        return redirect('/films')
    return render(request, 'movies/pages/delete.html', {'object': movie})

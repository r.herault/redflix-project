from django.core.management.base import BaseCommand, CommandError

# Scraping
import requests
from bs4 import BeautifulSoup

from movies.models import Movie, Director, Actor, Genre, ActorMovie, GenreMovie

class Command(BaseCommand):
    help = 'Scraping AlloCine to fill database'

    def add_arguments(self, parser):
        parser.add_argument('url', nargs='?', type=str, default='http://www.allocine.fr/film/meilleurs/')
        parser.add_argument('nb_movies', nargs='?', type=int, default=10)

    def handle(self, *args, **options):
        url = requests.get(options['url'])
        soup = BeautifulSoup(url.text, 'html.parser')
        for elem in soup.select('li.mdl > .entity-card')[:options['nb_movies']]:
            print('-------------------------------------------')
            print('------ Titre -----')
            print(elem.select_one('h2.meta-title > a.meta-title-link').contents)
            title = elem.select_one('h2.meta-title > a.meta-title-link').contents

            print('----- Réalisateur -----')
            print(elem.select_one('.meta-body-direction > a').contents)
            director = elem.select_one('.meta-body-direction > a').contents
            director_split = director[0].split(' ')
            movie_director = Director.objects.create(name=director_split[0], firstname=director_split[1])

            print('----- Synopsis -----')
            if elem.select_one('.synopsis') != None :
                print(elem.select_one('.synopsis > .content-txt').contents)
                summary = elem.select_one('.synopsis > .content-txt').contents
            else :
                print('Ce film ne possède pas de synopsis')

            print('----- Note -----')
            if elem.select_one('.stareval > span.stareval-note') != None :
                print(elem.select_one('.stareval > span.stareval-note').contents)
                note = elem.select_one('.stareval > span.stareval-note').contents
                note_abs = note[0].replace(',', '.')
            else :
                print('Ce film ne possède pas de note')
                note_abs = 0

            print('----- Bande annonce -----')
            print(elem.select_one('.thumbnail-link').attrs)
            trailer = elem.select_one('.thumbnail-link').attrs

            print('----- Poster -----')
            print(elem.select_one('.thumbnail-img').get('data-src'))
            poster_url = elem.select_one('.thumbnail-img').get('data-src')
            if poster_url is None :
                poster_url = 'https://i.pinimg.com/originals/72/24/f6/7224f6d53614cedbf8cef516b705a555.jpg'

            movie = Movie.objects.create(title=title[0], poster_url=poster_url, trailer='https://www.youtube.com/watch?v=LR5-LsMWRNg&t=552s', nationality='FR', release='2000', summary=summary[0], note=round(float(note_abs)), director=movie_director)

            print('----- Genre -----')
            for genre in elem.select('.meta-body-info > span:not(.spacer, .date)') :
                print(genre.contents)
                movie_genre = Genre.objects.create(name=genre.contents[0])
                GenreMovie.objects.create(genre=movie_genre, movie=movie)

            print('----- Acteurs -----')
            for actor in elem.select('.meta-body-actor > a') :
                print(actor.contents)
                actor_split = actor.contents[0].split(' ')
                movie_actor = Actor.objects.create(name=actor_split[0], firstname=actor_split[1])
                ActorMovie.objects.create(actor=movie_actor, movie=movie)

        self.stdout.write(self.style.SUCCESS('Successfully scraped'))

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 

class Director(models.Model):
  """
  Class Director
  """
  name = models.CharField(max_length=100)
  firstname = models.CharField(max_length=100)

  def __str__(self):
    return self.name + " " + self.firstname

class Actor(models.Model):
  name = models.CharField(max_length=100)
  firstname = models.CharField(max_length=100)

  def __str__(self):
    return self.name + " " + self.firstname

class Genre(models.Model):
  """
  Class Genre
  """
  name = models.CharField(max_length=200)

  def __str__(self):
    return self.name

class Movie(models.Model):
  """
  Class Movie
  """
  title = models.CharField(max_length=200)
  poster = models.ImageField(upload_to='images/', blank=True)
  poster_url = models.URLField(max_length=500, blank=True)
  trailer = models.URLField(max_length=500)
  nationality = models.CharField(max_length=3)
  release = models.IntegerField(default=2000)
  summary = models.TextField()
  note = models.IntegerField(default=5, validators=[MinValueValidator(1), MaxValueValidator(5)])
  pub_date = models.DateTimeField('Date de publication', auto_now=True)
  director = models.ForeignKey(Director, on_delete=models.CASCADE)

  def __str__(self):
    return self.title

class ActorMovie(models.Model):
      actor = models.ForeignKey(Actor, on_delete=models.CASCADE)
      movie = models.ForeignKey(Movie, on_delete=models.CASCADE)

class GenreMovie(models.Model):
      genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
      movie = models.ForeignKey(Movie, on_delete=models.CASCADE)

from django.urls import path

from . import views

app_name = 'movies'
urlpatterns = [
    path('', views.index, name='home'),
    path('films', views.listing, name='listing'),
    path('films/edit/<int:pk>', views.edit, name='edit'),
    path('films/add', views.add, name='add'),
    path('films/delete/<int:pk>', views.delete, name='delete'),
]

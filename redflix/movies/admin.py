from django.contrib import admin
from movies.models import *

admin.site.register(Genre)
admin.site.register(Director)
admin.site.register(Actor)
admin.site.register(Movie)
admin.site.register(ActorMovie)
admin.site.register(GenreMovie)
from django.apps import AppConfig


class MoviesConfig(AppConfig):
    name = 'movies'
    STATIC_URL = "/static/"

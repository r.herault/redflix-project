from django.test import TestCase, Client

class UsersTestCase(TestCase):
    def setUp(self):
        client = Client()

    def test_route_users_login_redirect_correctly(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_route_users_logout_redirect_correctly(self):
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)

from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'
    STATIC_URL = '/static/'

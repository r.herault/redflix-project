#!/bin/bash

read -p "Voulez-vous (re)créer le virtualenv ?[y/n]" reponse
if echo "$reponse" | grep -iq "^y"; then
    rm -rf venv
    python3 -m venv venv
fi

echo -e "\033[1;41mActivation du virtualenv...\033[0m"
source ./venv/bin/activate

echo -e "\033[1;41mInstallation des dépendances...\033[0m"
pip install -r requirements.txt

read -p "Voulez-vous lancer l'application ? [y/n]" reponse
if echo "$reponse" | grep -iq "^y"; then
    echo -e "\033[1;41mLancement de l'application...\033[0m"
    python3 redflix/manage.py runserver
fi
